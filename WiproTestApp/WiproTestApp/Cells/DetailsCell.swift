//
//  DetailsCell.swift
//  WiproTestApp
//
//  Created by Sriram Sadhasivan on 26/03/19.
//  Copyright © 2019 Sriram Sadhasivan. All rights reserved.
//

import UIKit

class DetailsCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var descLbl: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func ConfigureUserDataForCell(indexPath:IndexPath , details:Details) {
        if details.description  != nil
        {
            self.descLbl.text = details.description
        }
        else{
            self.descLbl.text = "NO DESCRIPTION AVAILABLE"
        }
        
        if details.title != nil
        {
            self.titleLabel.text = details.title
        }else{
            self.titleLabel.text = "NO TITLE AVAILABLE"
        }
        if(details.imgSrc != nil)
        {
            let url = URL(string: details.imgSrc!)!
            let placeholderImage = UIImage(named: "placeHolderImage.jpeg")!
            
            self.imgView.af_setImage(withURL: url, placeholderImage: placeholderImage)
            
        }else{
            self.imgView.image = UIImage(named:"placeHolderImage.jpeg")
        }
        
        
    }
}
