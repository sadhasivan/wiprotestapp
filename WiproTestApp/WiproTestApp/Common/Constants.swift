//
//  Constants.swift
//  WiproTestApp
//
//  Created by Sriram Sadhasivan on 26/03/19.
//  Copyright © 2019 Sriram Sadhasivan. All rights reserved.
//

import Foundation
struct Constants {
   
    //Urls
    static let aboutCanada = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json"
  
    
    
    
    //Alert and error Messages
    static let networkError = "No Network Connection Available."
    static let oKTitle = "OK"
    static let alertTitle = "Alert"
    static let titleNotAvail = "NO TITLE AVAILABLE"
    static let descNotAvail = "NO DESCRIPTION AVAILABLE"
    
    
    //Cell identifier
     static let detailsCell = "DetailsCell"
}
