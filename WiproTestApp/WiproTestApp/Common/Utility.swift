//
//  Utility.swift
//  WiproTestApp
//
//  Created by Sriram Sadhasivan on 27/03/19.
//  Copyright © 2019 Sriram Sadhasivan. All rights reserved.
//

import UIKit

class Utility: NSObject {
    
    
 class func height(withConstrainedWidth width: CGFloat, font: UIFont , text: String) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = text.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes:  [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }

}
