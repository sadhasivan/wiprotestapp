//
//  CanadaDetails.swift
//  WiproTestApp
//
//  Created by Sriram Sadhasivan on 26/03/19.
//  Copyright © 2019 Sriram Sadhasivan. All rights reserved.
//

import UIKit
import ObjectMapper

class Canada: Mappable {
    
    var title:String?
    var rows:[Details]?
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        rows  <- map["rows"]
    }
}
class Details : Mappable {
    var title:String?
    var description:String?
    var imgSrc:String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
         title  <- map["title"]
        description <- map["description"]
        imgSrc <- map["imageHref"]
        
    }
}
