//
//  CanadaListViewController.swift
//  WiproTestApp
//
//  Created by Sriram Sadhasivan on 26/03/19.
//  Copyright © 2019 Sriram Sadhasivan. All rights reserved.
//

import UIKit
import ObjectMapper
import AlamofireImage

class CanadaListViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var networkLbl: UILabel!
    @IBOutlet weak var detailsTableView: UITableView!
    var dataArray = [Details]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        print("Test")
        
        
        //Register tableview cell
     let nib = UINib(nibName: Constants.detailsCell, bundle: nil)
        detailsTableView.register(nib, forCellReuseIdentifier:Constants.detailsCell)
        detailsTableView.delegate = self
        detailsTableView.dataSource = self
        detailsTableView.isHidden = true
        hideNetworkLbl()
        
        // Network Check
        if(connectedToNetwork())
        {
            loadDetails()
        }
        else{
            showNetworkLbl()
            networkLbl.text = Constants.networkError
           alert(title: Constants.alertTitle, message:Constants.networkError, actionTitle: Constants.oKTitle)
        }
    }
    
    // MARK: TableView DataSource and Delegate Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier:String =  Constants.detailsCell
        let cell:DetailsCell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) as! DetailsCell
        
        DispatchQueue.global(qos: .userInitiated).async {
            let userdata = self.dataArray[indexPath.row]
            DispatchQueue.main.async {
               cell.ConfigureUserDataForCell(indexPath: indexPath, details: userdata)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let data = dataArray[indexPath.row]
        
        // title height
        let titleHeight = Utility.height(withConstrainedWidth: tableView.frame.size.width - 20, font: UIFont.systemFont(ofSize: 24.0, weight:UIFont.Weight.bold), text:data.title ?? Constants.titleNotAvail)
        
        //Description Height
        let descHeight = Utility.height(withConstrainedWidth: tableView.frame.size.width - 20, font: UIFont.systemFont(ofSize: 18.0, weight:UIFont.Weight.regular), text:data.description ?? Constants.descNotAvail)
        
        return titleHeight+descHeight+210
    }
   
     // MARK: WebService Calls
    
    func loadDetails() {
        
        RestService.RequestByMethod(methodType: .get, url: Constants.aboutCanada, accesstoken:nil, parameters: nil) { (response, error)  in
            if error == nil{
                if  let details = Mapper<Canada>().map(JSONString: response!)
                {
                if let rowData = details.rows
                    {
                        self.dataArray = rowData
                        DispatchQueue.main.async {
                            self.setNavigationTitle(title:details.title!)
                            self.hideNetworkLbl()
                            self.detailsTableView.isHidden = false
                            if self.dataArray.count>0{
                                self.detailsTableView.reloadData()
                            }
                        }
                    }
                }
            }
            else{
                print (error?.localizedDescription ?? "Error")
            }
        }
    }
    
    func hideNetworkLbl(){
           self.networkLbl.isHidden = true
    }
    func showNetworkLbl(){
           self.networkLbl.isHidden = false
    }

}
