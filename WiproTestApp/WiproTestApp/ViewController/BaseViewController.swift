//
//  BaseViewController.swift
//  WiproTestApp
//
//  Created by Sriram Sadhasivan on 27/03/19.
//  Copyright © 2019 Sriram Sadhasivan. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // Testing for devlopment Branch
    }
    
    func setNavigationTitle(title:String)
    {
        self.title = title
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        self.navigationController?.navigationBar.barTintColor = UIColor.darkGray
    }
    
    
    func  alert(title:String , message:String , actionTitle:String)
    {
    let alert = UIAlertController(title:title, message:message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title:actionTitle, style: .default, handler: { action in
        switch action.style{
        case .default:
            self.dismiss(animated: true, completion: nil)
            
        case .cancel:
            self.dismiss(animated: true, completion: nil)
            
        case .destructive:
            self.dismiss(animated: true, completion: nil)
            
            
        }}))
    self.present(alert, animated: true, completion: nil)
    }
}


