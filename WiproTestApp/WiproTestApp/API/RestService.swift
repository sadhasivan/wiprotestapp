//
//  RestService.swift
//  WiproTestApp
//
//  Created by Sriram Sadhasivan on 26/03/19.
//  Copyright © 2019 Sriram Sadhasivan. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper


class RestService: NSObject {
    typealias CompletionBlock = (_ result:String? , _ error:Error?) -> ()
    class func RequestByMethod(methodType:HTTPMethod , url:String , accesstoken:String?, parameters:[String:Any]? , ResponseBlock:@escaping CompletionBlock)
{
    
        let header:[String:String] = [
            "Content-Type": "text/plain"
        ]
        Alamofire.request(url, method: methodType, parameters: parameters, encoding:JSONEncoding.default , headers: header).responseString(completionHandler: { response in
            
            if response.response?.statusCode == 200
            {
                switch response.result{
                case .success:
                    
                    ResponseBlock(response.result.value,nil)
                    break
                    
                case .failure:
                    ResponseBlock(nil,response.result.error)
                    break
                }
            }
        })
 
    
}

}
